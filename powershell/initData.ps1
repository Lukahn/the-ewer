$indexTable = Import-Csv .\questsindex.csv
$fileTable = Import-Csv .\questsfiles.csv

$data = @()

foreach($line in $indexTable){
    if($line.journal -like "*xtx*"){
        $pos = $line.journal.IndexOf("xtx")
        $rightPart = $line.journal.Substring($pos)
        $pos = $rightPart.IndexOf(")")
        $leftPart = $rightPart.Substring(0,$pos)
        $line.journal = $leftPart
    }

    foreach($fileLine in $fileTable){
        if($line.id -eq $fileLine.id){

            Write-Host "ok" -ForegroundColor Green

            $dataLine = New-Object -TypeName PSObject -Property @{
                id      = $line.id
                jp      = $line.jp
                en      = $line.en
                de      = $line.de
                fr      = $line.fr
                journal = $line.journal
                file    = $fileLine.file
                category = $fileLine.category
            }

            $data+=$dataLine
        }
    }
}

$data | Export-Csv -Path ".\data.csv" -Encoding Unicode